# Calculadora de latas de tintas

Projeto desafio na Digital Republic. Desenvolvido com framework Nextjs.

## Instruções

Escolha um diretório de sua preferência e clone o repositório com o comando:

### `git clone https://gitlab.com/botelhojoao94/ink-calc-new.git`

Em seguida instale todas as dependências na raiz do projeto com o comando:

### `npm install`

Para iniciar o projeto insira o comando:

### `npm run dev`

A aplicação estará disponível em:

### `localhost:3000`

Autor: João Paulo Botelho
Link para o meu portfólio: [https://botelhojoao94.github.io/portfolio/](https://botelhojoao94.github.io/portfolio/)
