import { useContext } from 'react'
import { CalcContext } from '../../context/CalcContext'


const CalculatorResult = (props) => {

    const { data } = useContext(CalcContext)

    return (
        <div className="results-area">
            <h2>Quantidade necessária de tinta</h2>
            <p>{data.result.totalLitters.toFixed(2)} Litros</p>
            <h2>Lata de 18l</h2>
            <p>{data.result.can1} Unidades</p>
            <h2>Lata de 3.6l</h2>
            <p>{data.result.can2} Unidades</p>
            <h2>Lata de 2.5l</h2>
            <p>{data.result.can3} Unidades</p>
            <h2>Lata de 0.5l</h2>
            <p>{data.result.can4} Unidades</p>
            <div className="return-btn" onClick={() => { props.setShowResultField(false) }}>Voltar</div>
        </div>
    )
}

export default CalculatorResult