import { useContext } from 'react'
import { CalcContext } from '../../context/CalcContext'
import axios from 'axios'
import { ErrorMessage, Formik, Form, Field } from 'formik'
import * as yup from 'yup';

const CalculatorForm = (props) => {

    const { data, setData } = useContext(CalcContext)

    function handleSubmit(values) {
        axios.post('/api/calc', values)
            .then((res) => {
                setData(res.data)
                console.log(res.data)
                if(!res.data.error){
                    props.setShowResultField(true)
                }
            })
    }

    const validations = yup.object().shape({
        height1: yup.number().required("Campo obrigatório"),
        width1: yup.number().required("Campo obrigatório"),
        nwindow1: yup.number().integer("Apenas números inteiros"),
        ndoor1: yup.number().integer("Apenas números inteiros")

    })

    return (
        <>
            <Formik
                initialValues={{}}
                validationSchema={validations}
                onSubmit={handleSubmit}>
                <Form className="form">

                    <div className="form-wall">
                        <h3 className="wall-title">Parede 1</h3>
                        <div className="form-row">
                            <div>
                                <Field placeholder="Altura (m)" name="height1" type="number" />
                                <ErrorMessage component="span" name="height1" />
                            </div>
                            <div>
                                <Field placeholder="Largura (m)" name="width1" type="number" />
                                <ErrorMessage component="span" name="width1" />
                            </div>
                        </div>
                        <div className="form-row">
                            <div>
                                <Field placeholder="Quantidade de janelas" name="nwindow1" type="number" />
                                <ErrorMessage component="span" name="nwindow1" />
                            </div>
                            <div>
                                <Field placeholder="Quantidade de portas" name="ndoor1" type="number" />
                                <ErrorMessage component="span" name="ndoor1" />
                            </div>
                        </div>
                    </div>

                    <div className="form-wall">
                        <h3 className="wall-title">Parede 2</h3>
                        <div className="form-row">
                            <div>
                                <Field placeholder="Altura (m)" name="height2" type="number" />
                                <ErrorMessage component="span" name="height2" />
                            </div>
                            <div>
                                <Field placeholder="Largura (m)" name="width2" type="number" />
                                <ErrorMessage component="span" name="width2" />
                            </div>
                        </div>
                        <div className="form-row">
                            <div>
                                <Field placeholder="Quantidade de janelas" name="nwindow2" type="number" />
                                <ErrorMessage component="span" name="nwindow2" />
                            </div>
                            <div>
                                <Field placeholder="Quantidade de portas" name="ndoor2" type="number" />
                                <ErrorMessage component="span" name="ndoor2" />
                            </div>
                        </div>
                    </div>

                    <div className="form-wall">
                        <h3 className="wall-title">Parede 3</h3>
                        <div className="form-row">
                            <div>
                                <Field placeholder="Altura (m)" name="height3" type="number" />
                                <ErrorMessage component="span" name="height3" />
                            </div>
                            <div>
                                <Field placeholder="Largura (m)" name="width3" type="number" />
                                <ErrorMessage component="span" name="width3" />
                            </div>
                        </div>
                        <div className="form-row">
                            <div>
                                <Field placeholder="Quantidade de janelas" name="nwindow3" type="number" />
                                <ErrorMessage component="span" name="nwindow3" />
                            </div>
                            <div>
                                <Field placeholder="Quantidade de portas" name="ndoor3" type="number" />
                                <ErrorMessage component="span" name="ndoor3" />
                            </div>
                        </div>
                    </div>

                    <div className="form-wall">
                        <h3 className="wall-title">Parede 3</h3>
                        <div className="form-row">
                            <div>
                                <Field placeholder="Altura (m)" name="height4" type="number" />
                                <ErrorMessage component="span" name="height4" />
                            </div>
                            <div>
                                <Field placeholder="Largura (m)" name="width4" type="number" />
                                <ErrorMessage component="span" name="width4" />
                            </div>
                        </div>
                        <div className="form-row">
                            <div>
                                <Field placeholder="Quantidade de janelas" name="nwindow4" type="number" />
                                <ErrorMessage component="span" name="nwindow4" />
                            </div>
                            <div>
                                <Field placeholder="Quantidade de portas" name="ndoor4" type="number" />
                                <ErrorMessage component="span" name="ndoor4" />
                            </div>
                        </div>
                    </div>
                    {data.error ? <span>{data.msg}</span> : null}

                    <div className='form-footer'>
                        <button className="calculate-btn" type="submit">Calcular</button>
                    </div>
                </Form>
            </Formik>
        </>
    )
}

export default CalculatorForm