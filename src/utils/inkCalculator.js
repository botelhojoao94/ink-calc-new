import { responseClass } from "./responseClass";
import { wallClass } from "./wallClass";

let checkedWalls = 0
const windowSize = (2 * 1.2) // 2.4m
const doorHeight = 1.9 // 1.9m
const doorSize = (0.8 * doorHeight) // 1.52m
const canSize1 = 18 // 18l
const canSize2 = 3.6 // 3.6l
const canSize3 = 2.5 // 2.5l
const canSize4 = 0.5 // 0.5l

const wall1 = new wallClass();
const wall2 = new wallClass();
const wall3 = new wallClass();
const wall4 = new wallClass();

wall1.name = 'Parede 1'
wall2.name = 'Parede 2'
wall3.name = 'Parede 3'
wall4.name = 'Parede 4'

const response = new responseClass();

export default function inkCalculator(data) {

    wall1.height = Number(data.height1)
    wall1.width = Number(data.width1)
    wall1.nWindow = Number(data.nwindow1) || 0
    wall1.nDoor = Number(data.ndoor1) || 0
    wall2.height = Number(data.height2)
    wall2.width = Number(data.width2)
    wall2.nWindow = Number(data.nwindow2) || 0
    wall2.nDoor = Number(data.ndoor2) || 0
    wall3.height = Number(data.height3)
    wall3.width = Number(data.width3)
    wall3.nWindow = Number(data.nwindow3) || 0
    wall3.nDoor = Number(data.ndoor3) || 0
    wall4.height = Number(data.height4)
    wall4.width = Number(data.width4)
    wall4.nWindow = Number(data.nwindow4) || 0
    wall4.nDoor = Number(data.ndoor4) || 0

    console.log(wall1)
    console.log(wall2)
    console.log(wall3)
    console.log(wall4)

    calculatorStart(wall1, wall2, wall3, wall4)

    return response
}

const calculatorStart = (wall1, wall2, wall3, wall4) => {
    checkedWalls = 0
    if (checkedWalls === 0)
        checkSquareMeters(wall1)
    if (checkedWalls === 1)
        checkSquareMeters(wall2)
    if (checkedWalls === 2)
        checkSquareMeters(wall3)
    if (checkedWalls === 3)
        checkSquareMeters(wall4)
    if (checkedWalls === 4) {
        calculateCanOfPaint(wall1, wall2, wall3, wall4)
    }
}

const checkSquareMeters = (wall) => {

    // Nenhuma parede pode ter mais de 15 metros quadrados
    if (wall.height * wall.width > 15) {
        response.error = true
        response.msg = `${wall.name}: A parede não pode ter mais de 15 metros quadrados`
        response.result = null
        return
    }
    // Nenhuma parede pode ter menos de 1 metro quadrado
    else if (wall.height * wall.width < 1) {
        response.error = true
        response.msg = `${wall.name}: A parede não pode ter menos de 1 metro quadrado`
        response.result = null
        return
    }
    else
        checkWindowAndDoor(wall)

}

const checkWindowAndDoor = (wall) => {

    const totalWindowsSize = windowSize * wall.nWindow
    const totalDoorsSize = doorSize * wall.nDoor

    // O total de área das portas e janelas deve ser no máximo 50% da área de parede
    if ((totalWindowsSize + totalDoorsSize) > (wall.height * wall.width) / 2) {
        response.error = true
        response.msg = `${wall.name}: O total de área das portas e janelas deve ser no máximo 50% da área de parede`
        response.result = null
        return
    }
    // A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta    
    else if ((wall.nDoor > 0) && (wall.height < doorHeight + 0.30)) {
        response.error = true
        response.msg = `${wall.name}: A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta`
        response.result = null
        return
    }
    else
        getUsefulArea(wall, totalWindowsSize, totalDoorsSize)

}

const getUsefulArea = (wall, totalWindowsSize, totalDoorsSize) => {

    // Total de área útil da parede
    wall.usefulArea = (wall.height * wall.width) - totalWindowsSize - totalDoorsSize
    checkedWalls++

}

const calculateCanOfPaint = (wall1, wall2, wall3, wall4) => {

    const totalLiters = (wall1.usefulArea + wall2.usefulArea + wall3.usefulArea + wall4.usefulArea) / 5
    let canAmount1 = 0 // 18l
    let canAmount2 = 0 // 3.6l
    let canAmount3 = 0 // 2.5l
    let canAmount4 = 0 // 0.5l
    let round = 1

    const calculate = (quantidade, canSize) => {
        if (round < 5) {
            let newCanSize = 0
            let int = ~~(quantidade / canSize)
            let res = quantidade % canSize
            if (round === 1) {
                canAmount1 = int
                newCanSize = canSize2
            }
            if (round === 2) {
                canAmount2 = int
                newCanSize = canSize3
            }
            if (round === 3) {
                canAmount3 = int
                newCanSize = canSize4
            }
            if (round === 4) {
                if (res >= 0.09)
                    canAmount4 = int + 1
                else
                    canAmount4 = int
            }
            round++
            return calculate(res, newCanSize)
        } else {
            response.error = false
            response.msg = "Calculado com sucesso"
            response.result = {
                "totalLitters": totalLiters,
                "can1": canAmount1,
                "can2": canAmount2,
                "can3": canAmount3,
                "can4": canAmount4
            }
            return
        }
    }

    return calculate(totalLiters, canSize1)
}