import { useState, createContext } from 'react';

export const CalcContext = createContext();

export function CalcWrapper(props) {
    const [data, setData] = useState({})

    return (
        <CalcContext.Provider value={{ data, setData }}>
            {props.children}
        </CalcContext.Provider>
    );
}
