import { useState } from 'react';
import Image from 'next/image';
import { CalcWrapper } from '../context/CalcContext'
import CalculatorForm from '../components/CalculatorForm'
import CalculatorResults from '../components/CalculatorResults'
import can from '../assets/images/can.png'

const Calculator = () => {

    const [showResultField, setShowResultField] = useState(false)
    return (
        <div className="App">
            <div className="calculator-area">
                <div className="top-area">
                    <Image height={100} width={100} src={can} alt='can-of-paint'></Image>
                    <h1 className="title" >Calculadora de tinta</h1>
                </div>
                <CalcWrapper>
                    {!showResultField ?
                        <CalculatorForm showResultField={showResultField} setShowResultField={setShowResultField} /> :
                        <CalculatorResults showResultField={showResultField} setShowResultField={setShowResultField} />
                    }
                </CalcWrapper>
            </div>
        </div>
    )
}

export default Calculator