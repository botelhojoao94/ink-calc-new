import Link from 'next/link'

export default function Home() {
    return (
        <div className="Home">
            <h1>Olá! Essa é uma calculadora de latas de tintas</h1>
            <h2>Clique no botão abaixo para iniciar</h2>
            <Link href="/calculator">
                <div className="start-btn" >Iniciar</div>
            </Link>
        </div>
    )
}
