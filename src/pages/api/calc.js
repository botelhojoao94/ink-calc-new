import inkCalculator from '../../utils/inkCalculator'

export default function handler(req, res) {
    const data = req.body
    const result = inkCalculator(data)
    res.json(result)
}
